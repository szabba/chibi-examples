(define-syntax where
    (syntax-rules ()
      ((where expr bind ...) (let (bind ...) expr))))

A simple example of creating hygienic macros with syntax-rules. Notably,
it wouldn't work with the macro defined inside the `*.sld` file, but
does after moving it to a `*.scm` and including in the aforementioned
`*.sld`. I've noticed this pattern throughout the Chibi Scheme's lib
directory -- not sure about the reason though.

The example is a single-expression let with the bindings following the
expression.

```bash
[karol@localhost 13-03-29 18:02 chibi-examples/syntax-rules]> chibi-scheme
> (import (syntax-rules-example))
> (where x (x 4))
4
> (where (* x (+ y z))
         (x 2)
         (y 3)
         (z 4))
14
> %
```

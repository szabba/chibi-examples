#!/usr/bin/env bash

chibi-ffi hello.stub
gcc -std=c99 -fpic -Wall -Werror -c hello.c -o hello.o
gcc -std=c99 -fpic -Wall -Werror -shared hello.o -o libhello.so

This is just a tiny example in which I'm trying to figure out how to use
the Chibi Scheme FFI functionality.

```bash
[karol@localhost 13-03-28 23:57 Code/chibi-ffi-example]> ./build.sh 
[karol@localhost 13-03-28 23:59 Code/chibi-ffi-example]> chibi-scheme
> (import (hello))
> (printf "hey")
hey3
> %  
```

I couldn't just `load` the `.so` from the REPL, and a binding to
`include-shared` wasn't present in it so I used the module system.

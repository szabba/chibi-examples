An RPN calculator -- one of the more popular excercise programs for
beginners (of which I'm quite fond of).

The implementation relies heavily on higher order functions and the main
loop is utilises tail-recursion.

Apart from numbers and the basic arithmetic operations (`+`, `-`, `*`,
`/`) it understands the constants `pi` and `e`, can print (`?`) the
contents of the internal stack, pop (`x`) values from it, clear (`c`)
it. Also, `exp`, `sin`, `cos`, `tan` and `cot` are provided.

Any invalid input will cause a, more or less informative, error.

#!/usr/bin/env chibi-scheme
;; An RPN calculator

(import
  ;; Basic Scheme
  (scheme base)

  ;; Simple IO
  (scheme read)
  (scheme write)

  ;; Inexact math, contains sin and others
  (scheme inexact)

  ;; List utilities
  (srfi 1)

  ;; String ports
  (srfi 6)

  ;; Macros for partial application
  (srfi 26))

;; A few constants...
(define pi (* 2 (asin 1)))
(define e (exp 1))

;; Constructor for constant functions of arbitrary arity
(define (make-const c)
  (lambda who-cares c))

;; Helper for building strings
(define (with-open-output-string func)
  (let ((out (open-output-string)))
    (func out)
    (get-output-string out)))

;; Helper for creating display commands
(define (rpn-display message-constructor)
  (lambda (stack)
    (list (message-constructor stack)
          stack #t)))

;; Helper for creating non-display commands
(define (rpn-action acting-function)
  (lambda (stack)
    (list "" (acting-function stack) #t)))

;; Takes a non-quiting action and turns it into a quiting one
(define (rpn-quit action)
  (lambda (stack)
    (let ((response (action stack)))
      (let ((message (car response))
            (new-stack (cadr response)))
        (list message new-stack #f)))))

;; Produces the string used to present the stack to the user
(define (stack->string stack)
  (if (not (null? stack))
    (with-open-output-string
      (lambda (out)
        (display "Stack contains: " out)
        (for-each (lambda (e)
                    (display e out) (display " " out))
                  (reverse stack))
        (newline out)))
    "Stack empty\n"))

;; Given an atom (the command trying to be matched) and string, creates
;; an action reporting an error and quitng the calculator
(define (rpn-error cmd err-message)
  (rpn-quit
    (rpn-display
      (lambda (stack)
        (with-open-output-string
          (lambda (out)
            (display "Command " out)
            (display cmd out)
            (display " caused an error:" out)
            (newline out)
            (display err-message out)
            (newline out)
            (display (stack->string stack) out)))))))

;; Used for defining n-ary operators for the calculator
(define (rpn-operator cmd op arity)
  (rpn-action
    (lambda (stack)
      (let ((len (length stack)))

        (if (> arity len)

          (rpn-action
            (lambda (stack)
              (let ((args (reverse (take stack arity)))
                    (rest (drop stack arity)))
                (cons (apply op args)
                      rest))))

          (begin
            ((rpn-error cmd
                        (with-open-output-string
                          (lambda (out)
                            (display "The operator requires " out)
                            (display arity out)
                            (display " arguments, and the stack is only " out)
                            (display len out)
                            (display " elements deep!" out))))
             stack)
            stack))))))

(define (rpn-operator cmd op arity)
  (rpn-action
    (lambda (stack)
      (let ((args (reverse (take stack arity)))
            (rest (drop stack arity)))
        (cons (apply op args) rest)))))

;; Used for defining single variable functions
(define rpn-func (cut rpn-operator <> <> 1))

;; Used for defining binary operators
(define rpn-binop (cut rpn-operator <> <> 2))

;; Clears the stack
(define clear
  (rpn-action (lambda (stack)
                (list))))

;; Pops an item off the stack
(define pop
  (rpn-action (lambda (stack)
            (if (null? stack)
              stack
              (cdr stack)))))

;; Pushes an item onto the stack
(define (push num)
  (rpn-action (lambda (stack)
                (cons num stack))))

;; Prints the stack
(define print (rpn-display stack->string))

;; Tries to match an atom to an action
(define (match-cmd cmd)
  (cond ((number? cmd) (push cmd))
        ((eq? cmd 'pi) (push pi))
        ((eq? cmd 'e) (push e))

        ((eq? cmd '+) (rpn-binop '+ +))
        ((eq? cmd '-) (rpn-binop '- -))
        ((eq? cmd '*) (rpn-binop '* *))
        ((eq? cmd '/) (rpn-binop '/ /))

        ((eq? cmd 'exp) (rpn-func 'exp exp))
        ((eq? cmd 'sin) (rpn-func 'sin sin))
        ((eq? cmd 'cos) (rpn-func 'cos cos))
        ((eq? cmd 'tan) (rpn-func 'tan tan))
        ((eq? cmd 'cot) (rpn-func 'cot (lambda (x) (/ (tan x)))))

        ((eq? cmd 'x) pop)
        ((eq? cmd 'c) clear)
        ((eq? cmd '?) print)
        ((eq? cmd 'q) (rpn-quit print))

        ((or (null? cmd) (pair? cmd))
         (rpn-error cmd "Commands must be Lisp atoms!"))
        (else (rpn-error cmd "Unknown command!"))))

;; The RPN calculator main loop function
(define (rpn-loop message stack go-on)
    (display message)
    (when go-on
      (let ((cmd (read)))
        (apply rpn-loop ((match-cmd cmd) stack)))))

;; Entry point for the main loop
(define (rpn)
  (rpn-loop "" (list) #t))

;; Runs the calculator
(rpn)

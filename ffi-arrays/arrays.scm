;; Element type registry
(define *types* #())
(define *types-count* 0)
(define *types-next-size* 1)

(define (resize-type-registry)

  (let ((new-types (make-vector *types-next-size*))
        (len (vector-length *types*)))

    (when (> len 0)
      (vector-copy! new-types 0 *types*))

    (set! *types* new-types))

  (set! *types-next-size* (* 2 *types-next-size*)))

;; Register a new array element type and return it's type-no
(define (register-element-type byte-size
                               write-over-pointer
                               read-from-pointer
                               fill)
  
  (when (= *types-count* (vector-length *types*))
    (resize-type-registry))

  (vector-set! *types* *types-count*
               (list byte-size
                     write-over-pointer
                     read-from-pointer
                     fill))

  (set! *types-count* (+ 1 *types-count*))

  (- *types-count* 1))

;; Accesing information about element types
(define (type-by-no type-no)
  (vector-ref *types* type-no))

(define (type-byte-size type-no)
  (list-ref (type-by-no type-no) 0))

(define (type-writer type-no)
  (list-ref (type-by-no type-no) 1))

(define (type-reader type-no)
  (list-ref (type-by-no type-no) 2))

(define (type-fill type-no)
  (list-ref (type-by-no type-no) 3))

;; A predicate telling whether an index or index pair is in- or outside
;; of the given arrays bounds
;;
;; With a single index tells you whether it's a valid index for the
;; array
;;
;; With two -- tells you wheter they form a valid [i; j[ index interval.
(define in-c-array-bounds?
  (case-lambda

    ((arr i)

     (and (<= 0 i) (< i (c-array-length arr))))

    ((arr i j)

     (<= 0 i j (c-array-length arr)))))

;; Element setter for C arrays
(define (c-array-set! arr i value)
  (let ((locus (c-array-ref-primitive arr i))
        (write-over-pointer (type-writer (c-array-typeno arr))))

    (write-over-pointer locus value)))

;; Element getter for C arrays
(define (c-array-ref arr i) 
  (let ((locus (c-array-ref-primitive arr i))
        (read-from-pointer (type-reader (c-array-typeno arr))))

    (read-from-pointer locus)))

;; Length of a C array
;;
;; Rename for exporting purposes
(define c-array-length c-array-size)

;; Fills a C array or it subset with the value given
(define c-array-fill!
  (case-lambda

    ((arr fill start end)

     (when (not (in-c-array-bounds? arr start end))
       (error "invalid array indices"))

     (let loop ((i start))
       (when (< i end)
         (c-array-set! arr i fill)
         (loop (+ i 1)))))

    ((arr fill start)

     (c-array-fill! arr fill start (c-array-length arr)))

    ((arr fill)

     (c-array-fill! arr fill 0))))

;; Constructor for C arrays
(define make-c-array
  (case-lambda

    ((type-no size fill)

     (let* ((byte-size (type-byte-size type-no))
            (data (malloc (* size byte-size)))
            (arr (c-array)))

       (c-array-typeno-set! arr type-no)
       (c-array-byte-size-set! arr byte-size)
       (c-array-size-set! arr size)
       (c-array-data-set! arr data)

       (c-array-fill! arr fill)

       arr))

    ((type-no size)

     (make-c-array type-no size (type-fill type-no)))))

;; Integer element type
(define element/int (register-element-type size-int
                                           write-int
                                           read-int
                                           0))

(define element/float (register-element-type size-float
                                             write-float
                                             read-float
                                             0.0))

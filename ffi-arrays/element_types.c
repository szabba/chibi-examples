#include <stddef.h>
#include "element_types.h"


const size_t SIZE_INT = sizeof(int);


int read_int(void* l) {

    if (l != NULL) {

        int* l2 = (int*) l;

        return *l2;
    }

    return 0;
}

void write_int(void* l, int v) {

    if (l != NULL) {

        int* l2 = (int*) l;

        *l2 = v;
    }
}


const size_t SIZE_FLOAT = sizeof(float);


float read_float(void* l) {

    if (l != NULL) {

        float* l2 = (float*) l;

        return *l2;
    }

    return 0;
}

void write_float(void* l, float v) {

    if (l != NULL) {

        float* l2 = (float*) l;

        *l2 = v;
    }
}

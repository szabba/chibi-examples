#ifndef C_ARRAYS_H
#define C_ARRAYS_H


typedef struct c_array {

    size_t typeno;
    size_t byte_size;
    size_t size;
    void* data;

} c_array;


void c_array_free(c_array* arr);

void* c_array_ref_primitive(c_array* arr, int i);


#endif

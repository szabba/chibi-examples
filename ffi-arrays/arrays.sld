(define-library (arrays)

  (export make-c-array c-array? c-array-length

          c-array-ref c-array-set!
          c-array-fill!

          register-element-type
          
          element/int element/float)

  (import (scheme base)
          (scheme case-lambda)
          (srfi 26))

  (include-shared "arrays")
  (include "arrays.scm"))

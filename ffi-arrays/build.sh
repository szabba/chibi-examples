#!/usr/bin/env bash

chibi-ffi arrays.stub
gcc -std=c99 -fpic -Wall -c c_arrays.c -o c_arrays.o
gcc -std=c99 -fpic -Wall -c element_types.c -o element_types.o
gcc -std=c99 -fpic -Wall -c arrays.c -o arrays.o
gcc -std=c99 -fpic -Wall -shared -L. -lchibi-scheme \
    arrays.o c_arrays.o element_types.o -o arrays.so

#include <stddef.h>
#include <stdlib.h>
#include "c_arrays.h"


void c_array_free(c_array* arr) {

    if (arr->data != NULL) {

        free(arr->data);
    }
}

void* c_array_ref_primitive(c_array* arr, int i) {

    if (i < 0 || arr->size <= i) {

        return NULL;

    } else {

        int offset = i * arr->byte_size;
        char* bytes = (char*) arr->data;

        return (void*) (bytes + offset);
    }
}

#ifndef ELEMENT_TYPES_H
#define ELEMENT_TYPES_H


#include <stdlib.h>


extern const size_t SIZE_INT;

int read_int(void* l);

void write_int(void* l, int v);


extern const size_t SIZE_FLOAT;

float read_float(void* l);

void write_float(void* l, float v);


#endif

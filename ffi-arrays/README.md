```bash
[karol@localhost 13-04-02 17:00 chibi-examples/ffi-arrays]> ./build.sh 
arrays.c: In function ‘sexp_c_array_ref_primitive_stub’:
arrays.c:48:9: warning: unused variable ‘tmp’ [-Wunused-variable]
arrays.c: In function ‘sexp_malloc_stub’:
arrays.c:59:9: warning: unused variable ‘tmp’ [-Wunused-variable]
[karol@localhost 13-04-02 17:00 chibi-examples/ffi-arrays]> chibi-scheme example.scm 
(define xs (make-c-array element/int 10 0))
(c-array-fill! xs 7 2 8)
(= (c-array-length xs) 10)
(= (c-array-ref xs 0) 0)
(= (c-array-ref xs 1) 0)
(= (c-array-ref xs 2) 7)
(= (c-array-ref xs 3) 7)
(= (c-array-ref xs 4) 7)
(= (c-array-ref xs 5) 7)
(= (c-array-ref xs 6) 7)
(= (c-array-ref xs 7) 7)
(= (c-array-ref xs 8) 0)
(= (c-array-ref xs 9) 0)
```

The warnings reported by GCC come from the file automatically generated
by chibi-ffi.

The library (`(arrays)`) exports an API loosely modeled after the vector
API from `(scheme base)`.

```scheme
(make-c-array element/type size)
(make-c-array element/type size fill)
```

Creates a C array of type specified by element/type (a non-negative
integer returned by a call to `register-element-type`) containing `size`
elements. With the optional `fill` argument all the elements get
initialised to it's value. Without it, the type's default fill value is
used.

```scheme
(c-array? arr)
```

Tells whether arr is a C array.

```scheme
(c-array-length arr)
```

Returns the length of `arr`.

```scheme
(c-array-ref arr i)
```

Returns the `i`-th element of array `arr`. When `i` is out of bounds,
the behaviour is unspecified.

```scheme
(c-array-set! arr i value)
```

Sets the `i`-th alement of `arr` to `value`. When `i` is out of bounds,
the behaviour is unspecified.

```scheme
(c-array-fill! arr fill start end)
(c-array-fill! arr fill start)
(c-array-fill! arr fill)
```

Fills the subsequence of `arr` from `start` (inclusive) to `end`
(exclusive) with `fill`. `start` defaults to `0` and `end` -- to
`(c-array-length arr)`.

```scheme
(register-element-type byte-size write-over-pointer read-from-pointer
fill)
```

Registers a new element type for C arrays as operated by the library.
`byte-size` has to be the value of sizeof returned for the type being
registered. `write-over-pointer` is a C function taking a void pointer
and a value of the given type and writing the value into the pointer.
`read-from-pointer` should take a void pointer and return the value it
refers to. `fill` is the value to which the elements get initialised by
default.

The value returned is the type number as required by `make-c-array`.

```scheme
element/int
```

Specifies an array of C ints when passed to `make-c-array`.

```scheme
element/float
```

Specifies an array of C floats when passed to `make-c-array`.

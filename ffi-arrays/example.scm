(import (scheme base)
        (scheme write)
        (arrays))

(display "(define xs (make-c-array element/int 10 0))") (newline)

(define xs (make-c-array element/int 10 0))

(display "(c-array-fill! xs 7 2 8)") (newline)

(c-array-fill! xs 7 2 8)

(display "(= (c-array-length xs) ")
(display (c-array-length xs))
(display ")") (newline)

(let loop ((i 0))
  (when (< i (c-array-length xs))

    (display "(= (c-array-ref xs ")
    (display i)
    (display ") ")
      ;;(display "BEFORE ERROR 2\n")
    (display (c-array-ref xs i))
      ;;(display "AFTER ERROR 2\n")
    (display ")") (newline)

    (loop (+ i 1))))
